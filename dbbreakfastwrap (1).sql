-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2015 at 06:32 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbbreakfastwrap`
--

-- --------------------------------------------------------

--
-- Table structure for table `tcode`
--

CREATE TABLE IF NOT EXISTS `tcode` (
`CODEID` int(11) NOT NULL,
  `CODEURL` varchar(255) DEFAULT NULL,
  `CODEACTIVE` int(11) DEFAULT NULL,
  `CODEYOUTUBE` varchar(255) DEFAULT NULL,
  `CREATEDATE` date DEFAULT NULL,
  `EXPIREDDATE` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tcode`
--

INSERT INTO `tcode` (`CODEID`, `CODEURL`, `CODEACTIVE`, `CODEYOUTUBE`, `CREATEDATE`, `EXPIREDDATE`) VALUES
(1, 'be40287513d411b4e891339023a291cb', 1, 'sdhbSGMTndY', '2015-03-31', '2015-04-01'),
(13, 'abcdefghijklmnopqrstuvwxyz', 1, NULL, '2015-04-01', '2015-04-06');

-- --------------------------------------------------------

--
-- Table structure for table `tdata`
--

CREATE TABLE IF NOT EXISTS `tdata` (
`DATAID` int(11) NOT NULL,
  `DATAEMAIL` varchar(255) DEFAULT NULL,
  `DATAIPADDRESS` varchar(255) DEFAULT NULL,
  `DATADATE` date DEFAULT NULL,
  `DATATIME` time DEFAULT NULL,
  `DATAVOUCHERCODE` varchar(255) DEFAULT NULL,
  `DATAUSEDATE` date DEFAULT NULL,
  `DATAUSETIMESTART` time DEFAULT NULL,
  `DATAUSETIMEEND` time DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tdata`
--

INSERT INTO `tdata` (`DATAID`, `DATAEMAIL`, `DATAIPADDRESS`, `DATADATE`, `DATATIME`, `DATAVOUCHERCODE`, `DATAUSEDATE`, `DATAUSETIMESTART`, `DATAUSETIMEEND`) VALUES
(2, 'dedenabss@gmail.com', '::1', '2015-03-31', '15:52:33', '0B73EE550A59', '2015-04-01', '06:00:00', '10:00:00'),
(3, 'dedenabs@gmail.com', '::1', '2015-03-31', '15:59:19', 'B374B099D213', '2015-04-01', '06:00:00', '10:00:00'),
(4, 'ririn1@gmail.com', '::1', '2015-04-01', '15:38:21', '3EB837787E83', '2015-04-02', '06:00:00', '10:00:00'),
(5, 'ririn2@gmail.com', '::1', '2015-04-01', '15:43:13', '5E0674EF170F', '2015-04-02', '06:00:00', '10:00:00'),
(6, 'ririn3@gmail.com', '::1', '2015-04-01', '15:43:20', 'ABBEF53AD7D7', '2015-04-02', '06:00:00', '10:00:00'),
(7, 'ririn4@gmail.com', '::1', '2015-04-01', '15:43:24', 'BF09E043E88C', '2015-04-02', '06:00:00', '10:00:00'),
(8, 'ririn5@gmail.com', '::1', '2015-04-01', '15:43:28', 'B4D266758C48', '2015-04-02', '06:00:00', '10:00:00'),
(9, 'ririn6@gmail.com', '::1', '2015-04-01', '15:43:33', 'FDFB29DB5045', '2015-04-02', '06:00:00', '10:00:00'),
(10, 'ririn7@gmail.com', '::1', '2015-04-01', '15:43:37', 'E74B93C201E2', '2015-04-02', '06:00:00', '10:00:00'),
(11, 'ririn8@gmail.com', '::1', '2015-04-01', '15:43:41', '6443AC0F8466', '2015-04-02', '06:00:00', '10:00:00'),
(12, 'ririn9@gmail.com', '::1', '2015-04-01', '15:43:45', '97754A86C93F', '2015-04-02', '06:00:00', '10:00:00'),
(13, 'ririn10@gmail.com', '::1', '2015-04-01', '15:43:49', '3DB5A4E73E31', '2015-04-02', '06:00:00', '10:00:00'),
(14, 'dedenabsa@gmail.com', '::1', '2015-04-01', '16:39:22', 'AAC32F59BBAC', '2015-04-02', '06:00:00', '10:00:00'),
(15, 'dedenabsas@gmail.com', '::1', '2015-04-01', '16:41:44', 'F3E09BA599F0', '2015-04-02', '06:00:00', '10:00:00'),
(16, 'dedennnabs@gmail.com', '::1', '2015-04-01', '16:42:07', '6ABA0273B30E', '2015-04-02', '06:00:00', '10:00:00'),
(17, 'dedenabaaaas@gmail.com', '::1', '2015-04-02', '12:17:14', '18A341FE34A5', '2015-04-03', '06:00:00', '10:00:00'),
(299, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:10:59', 'BBDFC8821FD0', '2015-04-10', '06:00:00', '10:00:00'),
(300, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:10:59', 'BBDFC8821FD0', '2015-04-10', '06:00:00', '10:00:00'),
(301, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:10:59', 'BBDFC8821FD0', '2015-04-10', '06:00:00', '10:00:00'),
(302, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:10:59', 'BBDFC8821FD0', '2015-04-10', '06:00:00', '10:00:00'),
(303, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:10:59', 'BBDFC8821FD0', '2015-04-10', '06:00:00', '10:00:00'),
(304, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:12:41', 'DFCF34718DDC', '2015-04-10', '06:00:00', '10:00:00'),
(305, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:12:41', 'AF3844E04EB5', '2015-04-10', '06:00:00', '10:00:00'),
(306, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:12:41', 'C41FC3DF5BE8', '2015-04-10', '06:00:00', '10:00:00'),
(307, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:12:41', '31F03CB1AD8F', '2015-04-10', '06:00:00', '10:00:00'),
(308, 'no-reply@mcdonalds.co.id', '::1', '2015-04-02', '17:12:41', '33554466519F', '2015-04-10', '06:00:00', '10:00:00'),
(309, 'deaaadenabs@gmail.com', '::1', '2015-04-02', '17:34:45', 'AFEC9F47079E', '2015-04-03', '06:00:00', '10:00:00'),
(310, 'deaasdasaadenabs@gmail.com', '::1', '2015-04-02', '17:35:28', '4AA7CD34808C', '2015-04-03', '06:00:00', '10:00:00'),
(311, 'ddeaasdasaadenabs@gmail.com', '::1', '2015-04-02', '17:36:31', '0CD346F840A7', '2015-04-03', '06:00:00', '10:00:00'),
(312, 'a@gmailcom', '::1', '2015-04-02', '17:47:15', '1A3406965E51', '2015-04-03', '06:00:00', '10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ttrackingcode`
--

CREATE TABLE IF NOT EXISTS `ttrackingcode` (
`TRACKINGCODEID` int(11) NOT NULL,
  `TRACKINGCODEURL` varchar(255) DEFAULT NULL,
  `TRACKINGCODEIP` varchar(255) DEFAULT NULL,
  `TRACKINGCODEDATE` date DEFAULT NULL,
  `TRACKINGCODETIME` time DEFAULT NULL,
  `TRACKINGCODEREFERENCE` varchar(255) DEFAULT NULL,
  `TRACKINGCODESTATUS` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ttrackingcode`
--

INSERT INTO `ttrackingcode` (`TRACKINGCODEID`, `TRACKINGCODEURL`, `TRACKINGCODEIP`, `TRACKINGCODEDATE`, `TRACKINGCODETIME`, `TRACKINGCODEREFERENCE`, `TRACKINGCODESTATUS`) VALUES
(1, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-01', '16:33:17', 'http://localhost/breakfastwrap/admin/videocode/', 'click'),
(2, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-01', '16:36:47', 'http://localhost/breakfastwrap/admin/videocode/', 'click'),
(3, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-01', '16:38:27', 'http://localhost/breakfastwrap/admin/videocode/', 'click'),
(5, '0', '::1', '2015-04-01', '16:41:44', 'http://localhost/breakfastwrap/?ref=be40287513d411b4e891339023a291cb', 'register'),
(6, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-01', '16:41:54', 'http://localhost/breakfastwrap/admin/videocode/', 'click'),
(7, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-01', '16:42:07', 'http://localhost/breakfastwrap/?ref=be40287513d411b4e891339023a291cb', 'register'),
(8, 'abcdefghijklmnopqrstuvwxyz', '::1', '2015-04-01', '16:42:34', 'http://localhost/breakfastwrap/admin/savevideocode/', 'click'),
(9, 'asdsad', '::1', '2015-04-01', '16:44:32', 'http://localhost/breakfastwrap/admin/videocode', 'click'),
(10, '1234567890', '::1', '2015-04-01', '16:44:38', 'http://localhost/breakfastwrap/admin/videocode', 'click'),
(11, 'asdsad', '::1', '2015-04-01', '16:44:40', 'http://localhost/breakfastwrap/admin/videocode', 'click'),
(12, 'abcdefghijklmnopqrstuvwxyz', '::1', '2015-04-01', '16:44:43', 'http://localhost/breakfastwrap/admin/videocode', 'click'),
(13, 'abcdefghijklmnopqrstuvwxyz', '::1', '2015-04-01', '16:44:46', 'http://localhost/breakfastwrap/admin/videocode', 'click'),
(14, '0', '::1', '2015-04-02', '12:15:52', 'No referrer set', 'click'),
(15, 'abcdefghijklmnopqrstuvwxyz', '::1', '2015-04-02', '12:16:49', 'http://localhost/breakfastwrap/admin/videocode/', 'click'),
(16, 'abcdefghijklmnopqrstuvwxyz', '::1', '2015-04-02', '12:17:01', 'http://localhost/breakfastwrap/admin/videocode/', 'click'),
(17, 'abcdefghijklmnopqrstuvwxyz', '::1', '2015-04-02', '12:17:14', 'http://localhost/breakfastwrap/?ref=abcdefghijklmnopqrstuvwxyz', 'register'),
(18, '0', '::1', '2015-04-02', '17:29:40', 'http://localhost/breakfastwrap/admin/voucherdata/', 'click'),
(19, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-02', '17:29:45', 'No referrer set', 'click'),
(20, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-02', '17:34:45', 'http://localhost/breakfastwrap/?ref=be40287513d411b4e891339023a291cb', 'register'),
(21, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-02', '17:35:28', 'http://localhost/breakfastwrap/?ref=be40287513d411b4e891339023a291cb', 'register'),
(22, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-02', '17:36:31', 'http://localhost/breakfastwrap/?ref=be40287513d411b4e891339023a291cb', 'register'),
(23, 'be40287513d411b4e891339023a291cb', '::1', '2015-04-02', '17:47:15', 'http://localhost/breakfastwrap/?ref=be40287513d411b4e891339023a291cb', 'register'),
(24, '0', '::1', '2015-04-06', '11:31:01', 'No referrer set', 'click');

-- --------------------------------------------------------

--
-- Table structure for table `tuser`
--

CREATE TABLE IF NOT EXISTS `tuser` (
`USERID` int(11) NOT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `USERPASSWORD` varchar(255) DEFAULT NULL,
  `USERLEVEL` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuser`
--

INSERT INTO `tuser` (`USERID`, `USERNAME`, `USERPASSWORD`, `USERLEVEL`) VALUES
(1, 'adminbreakfastwrap', '0d502ae011cd1cb26cfea802358e7fe6', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tcode`
--
ALTER TABLE `tcode`
 ADD PRIMARY KEY (`CODEID`);

--
-- Indexes for table `tdata`
--
ALTER TABLE `tdata`
 ADD PRIMARY KEY (`DATAID`);

--
-- Indexes for table `ttrackingcode`
--
ALTER TABLE `ttrackingcode`
 ADD PRIMARY KEY (`TRACKINGCODEID`);

--
-- Indexes for table `tuser`
--
ALTER TABLE `tuser`
 ADD PRIMARY KEY (`USERID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tcode`
--
ALTER TABLE `tcode`
MODIFY `CODEID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tdata`
--
ALTER TABLE `tdata`
MODIFY `DATAID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=313;
--
-- AUTO_INCREMENT for table `ttrackingcode`
--
ALTER TABLE `ttrackingcode`
MODIFY `TRACKINGCODEID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tuser`
--
ALTER TABLE `tuser`
MODIFY `USERID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
