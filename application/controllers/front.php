<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
    	error_reporting(E_ALL ^ E_NOTICE); 
        parent::__construct();
		$this->load->model("mdata");
		$this->load->helper(array('form', 'url','file'));
		$this->load->helper('cookie');
		$this->load->library(array('session'));		
		date_default_timezone_set('Asia/Jakarta'); 
	}	


	function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0){        
	    if ($spacing == 0)
	    {
	        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
	    }
	    else
	    {
	        $temp_x = $x;
	        for ($i = 0; $i < strlen($text); $i++)
	        {
	            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
	            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
	        }
	    }
	}

	function getTodayVoucherDate(){
		$now			= date("Y-m-d H:i:s");
		$changedate			= date("Y-m-d 18:00:00");
		
		return ($now > $changedate? date("Y-m-d H:i:s",strtotime("+1 day")) : date("Y-m-d"));
		 
	}

	function sendEmail($path, $txtemail){
		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'smtp.mandrillapp.com',
		    'smtp_port' => 587,
		    'smtp_user' => 'leoburnettid.analytics@gmail.com',
		    'smtp_pass' => 'KLSAEwtDhOrIdZmxjYl9UA',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('no-reply@mcdonalds.co.id', 'McDonalds');
		$this->email->to($txtemail); 

		$this->email->subject("GRATIS VOUCHER BREAKFAST WRAP MCDONALD'S!");
			
		$pathimage = base_url().$path;
		$messages = "
		<html>
		<head>
		<title>GRATIS VOUCHER BREAKFAST WRAP MCDONALD'S!</title>
		</head>
		<body>
			<a href='".base_url().$path."'>Klik disini jika image tidak terbuka</a>
			<table width='80%' style='border:none; text-align:center;'>
				<tr>
					<td style='text-align:center;'><img alt='GAMBAR VOUCHER' src='".base_url().$path."' width='80%'></td>
				</tr>
			</table>
		</body>
		</html>
		";

		$this->email->message($messages);
		$this->email->send();
	}

	//PROVIDE TEMPLATE
	function selectTemplate($action){
		
		switch($action){

			case "HOME":
				$datenow 				= date("Y-m-d", strtotime($this->getTodayVoucherDate()));
				$timenow 				= date('H:i:s');						
				
				$data['cekavailable'] 	= 100-($this->mdata->cekavailable($datenow)->num_rows());
				if($data['cekavailable'] == 0){
					$this->selectTemplate("OUT OF STOCK");
					return;
				}
				$data['ipaddress'] 		= $ipaddress;
				$data['datenow'] 		= $datenow;
				$data['timenow'] 		= $timenow;
				$this->load->view('vindex',$data);
				break;

			case "SUCCESS":
				$data['title']		= 'Breakfast Wrap';
				$data['message1'] 	= 'TERIMA KASIH';
				$data['message2'] 	= '1 VOUCHER TELAH BERHASIL DIKIRIM';
				$data['message3'] 	= 'CEK EMAIL KAMU SEKARANG';
				$data['jpgsukses'] 	= 'assets/img/voucher/'.$vouchercode.'.jpg';
				$this->load->view('vsukses',$data);
				break;

			case "EXPIRED":
				$data['title'] 		= 'Breakfast Wrap';
				$data['message1'] 	= 'Ooops..';
				$data['message2'] 	= 'TAUTAN TELAH KADALUARSA';
				$data['message3'] 	= '';
				$this->load->view('verror',$data);
				break;

			case "ERROR":
				$data['title'] 		= 'Breakfast Wrap';
				$data['message1'] 	= 'Ooops..';
				$data['message2'] 	= 'NONTON DULU CERITANYA UNTUK DAPETIN VOUCHERNYA';
				$data['message3'] 	= '';
				$data['error'] 	= "1";
				$this->load->view('verror',$data);
				break;

			case "PARTICIPATED":
				$data['title'] 			= 'Breakfast Wrap';
				$data['message1'] 		= 'MAAF';
				$data['message2'] 		= 'KAMU SUDAH BERPARTISIPASI';
				$data['message3'] 		= 'SILAHKAN COBA LAGI BESOK';
				$data['participated'] 	= "1";
				$this->load->view('verror',$data);
				break;

			case "OUT OF STOCK":
				$data['title'] = 'Breakfast Wrap';
				$data['message1'] = 'MAAF';
				$data['message2'] = 'VOUCHER SUDAH HABIS';
				$data['message3'] = 'SILAHKAN COBA LAGI BESOK';
				$this->load->view('verror',$data);
				break;
		}
	}

	function saveDataTracking($codeURL, $ipaddress, $datenow, $timenow, $action){

		$this->load->model('mdata');
		
		$referer = ( isset($_SERVER['HTTP_REFERER'])?  $_SERVER['HTTP_REFERER']:'No referrer set' );

		$savedatatracking = array(
			'TRACKINGCODEURL' 		=> $codeURL,
			'TRACKINGCODEIP' 		=> $ipaddress,
			'TRACKINGCODEDATE' 		=> $datenow,
			'TRACKINGCODETIME' 		=> $timenow,
			'TRACKINGCODEREFERENCE' => $referer,
			'TRACKINGCODESTATUS' 	=> $action
		);

		$this->mdata->savedatatracking($savedatatracking);
				
	}

	public function index(){
	

		//IS USER COMING FROM YOUTUBE?
		/*if (strpos($_SERVER['HTTP_REFERER'], "http://youtube.com/") === false) {
		 	$this->load->library('user_agent');
		 	var_dump($this->agent->referrer());
			$this->selectTemplate("ERROR"); 
		 	return;
		}*/
		
	
		//HAS USER PARTICIPATED BEFORE?		
		if( get_cookie('voucher_status') != NULL ){	$this->selectTemplate("PARTICIPATED");	return;}

		//DO USER HAS THE CODE?
		$codeurl 		= $this->input->get('ref',true);
  		if($codeurl==NULL || $codeurl==''){ $this->selectTemplate("ERROR");	return;}

		date_default_timezone_set('Asia/Jakarta'); 

		$this->load->helper('date');

		$data['title'] 	= 'Breakfast Wrap';
		$datenow 		= date("Y-m-d", strtotime($this->getTodayVoucherDate()));
		$timenow 		= date('H:i:s');				
		$ipaddress 		= $this->input->ip_address();
		


		//SET REF SESSION
		$this->session->set_userdata('ref', $codeurl);
		

		//SAVE TRACKING DATA
		$this->saveDataTracking($codeurl, $ipaddress, $datenow, $timenow, 'click');

  		//USER HAS THE CODE AND THEN
		if( isset($codeurl) ){
			
			$query = $this->db->get_where('TCODE', array('CODEURL' => $codeurl));
			$count = $query->num_rows();

			//CHECK IF THE CODE IS VALID?
			if($count == 1){

				$row = $query->row();				
				$exp_date = strtotime($row->EXPIREDDATE);


				//IS THE CODE FOR TODAY?
				if($datenow == date("Y-m-d",$exp_date)){ $this->selectTemplate("HOME"); }					

				else{
					$this->selectTemplate("EXPIRED"); 
				}
			}
			else{ $this->selectTemplate("ERROR"); }
		}
	}


	function createVoucherImage(){
		
		//FONT FAMILY
		$fontname = 'assets/fonts/AkzidenzGroteskBECn.ttf';
		
		//SET JPG QUALITY
		$quality = 100;

		//DEFINE FILE NAME & LOCATION
		//$file = "assets/img/voucher/".$this->session->userdata('vouchercode').".jpg";	
		$voucherName = 	date("Y-m-d H.i.s")."-".$this->session->userdata('vouchercode');
		$file = "assets/img/voucher/".$voucherName.".jpg";	

			//1. DEFINING BASE IMAGE
			$valueMonth = strtoupper( date('m', strtotime($this->getTodayVoucherDate().' +1 day')) );			
			$vouchersTemplate = ($valueMonth==4?'assets/img/voucher-template.jpg':'assets/img/voucher-template-may.jpg');
			$im = imagecreatefromjpeg($vouchersTemplate);
			
			//2. EMBED VOUCHER CODE			
			$valuename = $this->session->userdata('vouchercode');
			$valuefontsize = '48';
			$valuecolor = imagecolorallocate($im, 88, 53, 18);
			$this->imagettftextSp($im, $valuefontsize, 0, 180, 777, $valuecolor, $fontname, $valuename, 10);

			//2.1 EMBED VOUCHER DATE
			$valuedate = strtoupper( date('d', strtotime($this->getTodayVoucherDate().' +1 day')) );
			$valuefontsize = '88';
			imagettftext($im, $valuefontsize, 0, 55, 690, $valuecolor, $fontname, $valuedate);
			
			//3. PRODUCE IMAGE
			imagejpeg($im, $file, 75);
						
		return $file;	
	}

	function checkEmailInput(){
		
		
		if($txtemail === FALSE){
			$this->selectTemplate("ERROR");
		}

		$this->load->helper(array('form', 'url'));
		$this->load->helper('email');

		$this->load->library('form_validation');
		$this->form_validation->set_message('required', 'Alamat email tidak boleh kosong');
		$this->form_validation->set_message('valid_email', 'Format email salah');
		$this->form_validation->set_rules('txtemail', 'Email', 'required|valid_email|xss_clean|trim');

		if ($this->form_validation->run() == FALSE)
		{
			$this->selectTemplate('HOME');
			return false;
		}
	}


	function getvoucher()
	{

		//DOES USER ACCESS THE SITE DIRECTLY?
		if (strpos($_SERVER['HTTP_REFERER'], base_url()) === false) { $this->selectTemplate("ERROR"); return;}

		//HAS USER PARTICIPATED BEFORE?
		if( get_cookie('voucher_status') != NULL ){	$this->selectTemplate("PARTICIPATED");	return;}


		//CHECK EMAIL INPUT
		$txtemail 	= 	$this->input->post("txtemail");
		$this->checkEmailInput($txtemail);


		$this->load->helper('date');
		date_default_timezone_set('Asia/Jakarta'); 


		$ipaddress 			= 	$this->input->ip_address();
		$datenow 			= 	date("Y-m-d", strtotime($this->getTodayVoucherDate()));
		$timenow 			= 	date('H:i:s');
		$usedate 			= 	date('Y-m-d',strtotime($datenow . "+1 days"));
		$vouchercode 		= 	strtoupper(substr(md5($txtemail.$datenow.$timenow),0,12));
		$codeURL 			= 	$this->session->userdata('ref');

		$cekdata 			= 	$this->mdata->cekdata($txtemail,$datenow)->num_rows();
		$times 				= 	'23:59:59'; 
		$time 				= 	strtotime($times)-strtotime($timenow); 


		//HAS USER PARTICIPATED?
		if( $cekdata>0 ){	$this->selectTemplate("PARTICIPATED"); }

		else{
			
			$cekavailable = $this->mdata->cekavailable($datenow)->num_rows();
			
			//IS VOUCHER OUT OF STOCK?
			if($cekavailable >= 100){ $this->selectTemplate("OUT OF STOCK"); }

			else{

				//SAVE USER DATA
				$savedata = array(
					'DATAEMAIL' 		=> $txtemail,
					'DATAIPADDRESS' 	=> $ipaddress,
					'DATADATE' 			=> $datenow,
					'DATATIME' 			=> $timenow,
					'DATAVOUCHERCODE' 	=> $vouchercode,
					'DATAUSEDATE' 		=> $usedate,
					'DATAUSETIMESTART' 	=> '06:00:00',
					'DATAUSETIMEEND' 	=> '09:00:00'
				);
				$this->mdata->savedata($savedata);
				
				//REMEMBER EMAIL AND VOUCHER VIA SESSION
				$this->load->library('session');
				$this->session->set_userdata('txtemail', $txtemail);
				$this->session->set_userdata('vouchercode', $vouchercode);
						
				//CREATE IMAGE
				$path = $this->createVoucherImage($user);

				//EMAIL USER
				$this->sendEmail( $path, $txtemail );				

				//SAVE DATA TRACKING
				$referer = ( isset($_SERVER['HTTP_REFERER'])?  $_SERVER['HTTP_REFERER']:'No referrer set' );
				$this->saveDataTracking($codeURL, $ipaddress, date("Y-m-d"), $timenow, 'register');

				//IMPLEMENT COOKIE
				$this->load->helper('cookie');

				// set cookie 
				setcookie("voucher_status", "claimed", time()+3600*5, "/");

				$this->selectTemplate("SUCCESS");
			}
			
		}

	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */