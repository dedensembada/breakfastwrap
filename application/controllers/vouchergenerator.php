<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vouchergenerator extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public $imageInProcess;

	public function __construct()
    {
    	error_reporting(E_ALL ^ E_NOTICE); 
    	date_default_timezone_set('Asia/Jakarta'); 
        parent::__construct();	
	}	


	function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0){        
	    if ($spacing == 0)
	    {
	        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
	    }
	    else
	    {
	        $temp_x = $x;
	        for ($i = 0; $i < strlen($text); $i++)
	        {
	            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
	            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
	        }
	    }
	}

	function sendEmail($path, $txtemail){
		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'smtp.mandrillapp.com',
		    'smtp_port' => 587,
		    'smtp_user' => 'leoburnettid.analytics@gmail.com',
		    'smtp_pass' => 'KLSAEwtDhOrIdZmxjYl9UA',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('no-reply@mcdonalds.co.id', 'McDonalds');
		$this->email->to($txtemail); 

		$this->email->subject("GRATIS VOUCHER BREAKFAST WRAP MCDONALD'S!");
			
		$pathimage = base_url().$path;
		$messages = "
		<html>
		<head>
		<title>GRATIS VOUCHER BREAKFAST WRAP MCDONALD'S!</title>
		</head>
		<body>
			<a href='".base_url().$path."'>Klik disini jika image tidak terbuka</a>
			<table width='80%' style='border:none; text-align:center;'>
				<tr>
					<td style='text-align:center;'><img alt='GAMBAR VOUCHER' src='".base_url().$path."' width='80%'></td>
				</tr>
			</table>
		</body>
		</html>
		";

		$this->email->message($messages);
		$this->email->send();
	}


	public function index(){
	}


	function createVoucherImage($vouchercode){
		date_default_timezone_set('Asia/Jakarta'); 
		//FONT FAMILY
		$fontname = 'assets/fonts/AkzidenzGroteskBECn.ttf';
		
		//SET JPG QUALITY
		$quality = 100;

		//DEFINE FILE NAME & LOCATION
		//$file = "assets/img/voucher/".$this->session->userdata('vouchercode').".jpg";	
		$voucherName = 	date("Y-m-d H.i.s")."-".$vouchercode;
		$file = "assets/img/voucher/".$voucherName.".jpg";	

			//1. DEFINING BASE IMAGE
			$valueMonth = strtoupper( date('m', strtotime(' +1 day')) );			
			$vouchersTemplate = ($valueMonth==4?'assets/img/voucher-template-twitter.jpg':'assets/img/voucher-template-twitter-may.jpg');
			$im = imagecreatefromjpeg($vouchersTemplate);
			
			//2. EMBED VOUCHER CODE			
			$valuename = $vouchercode;
			$valuefontsize = '48';
			$valuecolor = imagecolorallocate($im, 88, 53, 18);
			$this->imagettftextSp($im, $valuefontsize, 0, 180, 777, $valuecolor, $fontname, $valuename, 10);

			//2.1 EMBED VOUCHER DATE
			$valuedate = strtoupper( date('d', strtotime(' +1 day')) );
			$valuefontsize = '88';
			imagettftext($im, $valuefontsize, 0, 55, 690, $valuecolor, $fontname, $valuedate);
			
			//3. PRODUCE IMAGE
			imagejpeg($im, $file, 75);
		return $file;	
	}


	function generateByJSON($jsonfile)
	{
		$this->load->helper('date');
		
		date_default_timezone_set('Asia/Jakarta'); 
		$jsonfile = file_get_contents(base_url().'/assets/twitter-email/'.$jsonfile);
		$result = json_decode($jsonfile);
		
		$this->imageInProcess = false;

		$index = 0;

		foreach ($result->email as $index => $email) {

			echo $email."<br>";

			$datenow 			= 	date("Y-m-d");
			$timenow 			= 	date('H:i:s');
			$usedate 			= 	date('Y-m-d',strtotime($datenow . "+1 days"));
			$vouchercode 		= 	strtoupper(substr(md5($txtemail.$datenow.$timenow),0,12));

			$path = $this->createVoucherImage($vouchercode);

			//EMAIL USER
			$this->sendEmail( $path, $email );	

			sleep(1);

		}

		die('SUCCESS');

	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */