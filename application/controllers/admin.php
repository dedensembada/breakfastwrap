<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model('mdata');
		$this->load->helper(array('form', 'url','file'));
    }
	
	function index()
	{
		$data['title'] = 'Breakfast Wrap';
		$this->load->view('vadminlogin',$data);
	}	

	function login()
	{
		$txtusername = $this->input->post("txtusername");
		$txtpassword = md5($this->input->post("txtpassword"));
		$qcekusername = $this->db->query("SELECT * FROM TUSER WHERE USERNAME='$txtusername' AND USERPASSWORD='$txtpassword'");
		$valcekusername = $qcekusername->num_rows();
		if($valcekusername == 1){
			$row = $qcekusername->row();
			$userlevel = $row->USERLEVEL;
			$this->load->library('session');
			$this->session->set_userdata('username', $txtusername);
			$this->session->set_userdata('userlevel', $userlevel);
			redirect('admin/dashboard/','refresh');
		}else{
			$data['title'] = "Breakfast Wrap";
			$data['error'] = "<h5 style='color:#ff0000;'>Invalid Username or Password!<br>Please Try Again!</h5>";
			$this->load->view('vadminlogin',$data);
		}
	}

	function dashboard()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Breakfast Wrap";
			$data['username'] = $this->session->userdata('username');
			//$data['qdata'] = $this->mdata->getRegistration();
			$this->load->view('vadmindashboard',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	function videocode()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Breakfast Wrap";
			$data['username'] = $this->session->userdata('username');
			$data['getVideoCode'] = $this->mdata->getVideoCode();
			$this->load->view('vadminvideocode',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	function generatevoucher()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Breakfast Wrap";
			$data['username'] = $this->session->userdata('username');
			//$data['getVideoCode'] = $this->mdata->getVideoCode();
			$this->load->view('vadmingeneratevoucher',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	function voucherdata()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = "Breakfast Wrap";
			$data['username'] = $this->session->userdata('username');
			$data['getVoucherData'] = $this->mdata->getVoucherData();
			$this->load->view('vadminvoucherdata',$data);
		}else{
			redirect('admin','refresh');
		}
	}


	function addvideocode()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Breakfast Wrap';
			$this->load->view('vadminaddvideocode',$data);
		}else{
			redirect('admin','refresh');
		}
	}
	
	
	function savevideocode()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Breakfast Wrap';
			//$youtubecode = $this->input->post('txtyoutubecode');
			$urlcode = $this->input->post('txturlcode');
			$expireddate = $this->input->post('txtexpireddate');
			$datenow = date("Y-m-d");
			$datacontent = array(
					//'CODEYOUTUBE' => $youtubecode,
					'CODEURL' => $urlcode,
					'CODEACTIVE' => 1,
					'CREATEDATE' => $datenow,
					'EXPIREDDATE' => $expireddate
			);
			$this->mdata->saveVideoCode($datacontent);
			$data['getVideoCode'] = $this->mdata->getVideoCode();
			//$this->load->view('vadminvideocode',$data);
			$data['title'] = "Breakfast Wrap";
			$data['username'] = $this->session->userdata('username');
			$data['getVideoCode'] = $this->mdata->getVideoCode();
			redirect('admin/videocode','refresh');
		}else{
			redirect('admin','refresh');
		}
	}

	function deleteVideoCode()
	{
		if($this->session->userdata('username')!='' || $this->session->userdata('username')!=NULL){
			$data['title'] = 'Breakfast Wrap';
			$id = $this->input->post("codeID");
			$deldata = $this->mdata->deleteVideoCode($id);
			redirect('admin/videocode/');
		}else{
			redirect('admin','refresh');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('admin','refresh');
	}

	function getvoucher()
	{
		//http://breakfastwrap.mcdonalds.co.id/admin/getvoucher?usedate=2015-04-10&loop=5
		//http://localhost/breakfastwrap/admin/getvoucher?usedate=2015-04-10&loop=5
		
		$loop = $this->input->get('loop',true);
		
		for($i=1;$i<=$loop;$i++){
			$this->load->helper('date');
			date_default_timezone_set('Asia/Jakarta'); 
			
			$txtemail = 'no-reply@mcdonalds.co.id';
			
			$datausedate = $this->input->get('usedate',true);
			$codeurl = 'generatevoucher';
			$ipaddress = $this->input->ip_address();
			$datenow = date("Y-m-d");
			$timenow = date('H:i:s');
			$vouchercode = strtoupper(substr(md5($txtemail.$datenow.$timenow.$i),0,12));

			//$cekdata = $this->mdata->cekdata($txtemail)->num_rows();
			$times = '23:59:59'; 
			$time = strtotime($times)-strtotime($timenow); 
			$cookie_email_name = "cookie_email";
			$cookie_email_value = $txtemail;
			//setcookie($cookie_email_name, $cookie_email_value, time() + $time, "/");
			setcookie($cookie_email_name, $cookie_email_value, time() + 5, "/");

			$savedata = array(
				'DATAEMAIL' => $txtemail,
				'DATAIPADDRESS' => $ipaddress,
				'DATADATE' => $datenow,
				'DATATIME' => $timenow,
				'DATAVOUCHERCODE' => $vouchercode,
				'DATAUSEDATE' => $datausedate,
				'DATAUSETIMESTART' => '06:00:00',
				'DATAUSETIMEEND' => '10:00:00'
			);
			$this->mdata->savedata($savedata);
			
			$this->load->library('session');
			$this->session->set_userdata('txtemail', $txtemail);
			$this->session->set_userdata('vouchercode', $vouchercode);
			$user = array(

					array(
						'name'=> $datenow.$timenow, 
						'fontsize'=>'27',
						'color'=>'grey'),
						
					array(
						'name'=> $vouchercode,
						'fontsize'=>'16',
						'color'=>'grey'),
						
					array(
						'name'=> $txtemail,
						'fontsize'=>'13',
						'color'=>'green'
						)
						
				);		
					
			// run the script to create the image
			$filename = $this->create_image($user,$datausedate);
		}
	}

	function create_image($user,$datausedate){
		// link to the font file no the server
		$fontname = base_url().'assets/fonts/AkzidenzGroteskBECn.ttf';
		// controls the spacing between text
		$i=30;
		//JPG image quality 0-100
		$quality = 100;
		global $fontname;	
		global $quality;
		$file = "assets/img/voucher/".$this->session->userdata('vouchercode').".jpg";	
		$i=30;
		// if the file already exists dont create it again just serve up the original	
		//if (!file_exists($file)) {	
			

			// define the base image that we lay our text on
			$passjpg = 'assets/img/voucher-template-twitter.jpg';
			$im = imagecreatefromjpeg($passjpg);
			
			// setup the text colours
			$color['grey'] = imagecolorallocate($im, 54, 56, 60);
			$color['green'] = imagecolorallocate($im, 55, 189, 102);
			$color['red'] = imagecolorallocate($im, 255, 0, 0);
			$height = 550;
			// this defines the starting height for the text block
			$y = imagesy($im) - $height - 175;
			$y2 = imagesy($im) - $height - 275;
			 
		// loop through the array and write the text	
		//foreach ($user as $value){
			// center the text in our image - returns the x value
			$valuedate = strtoupper(date("d",strtotime($datausedate)));
			$valuename = $this->session->userdata('vouchercode');
			$valuefontsize = '32';
			$valuecolor = imagecolorallocate($im, 88, 53, 18);
			$x = $this->center_text($valuename, $valuefontsize);	
			$x2 = $this->center_text2($valuedate, $valuefontsize);	
			imagettftext($im, 34, 0, $x, $y+$i, $color[$valuecolor], $fontname,$valuename);
			imagettftext($im, 72, 0, $x2, $y2+$i, $color[$valuecolor], $fontname,$valuedate);
			// add 32px to the line height for the next text block
			$i = $i+32;	
			
		//}
			// create the image
			imagejpeg($im, $file, 75);
			
		//}
						
		return $file;	
	}

	function center_text($string, $font_size){

		global $fontname;
		$fontname = 'assets/fonts/Capriola-Regular.ttf';
		$image_width = 700;
		$dimensions = imagettfbbox($font_size, 20, $fontname, $string);
		
		return ceil(($image_width - $dimensions[4]) / 2);				
	}

	function center_text2($string, $font_size){

		global $fontname;
		$fontname = 'assets/fonts/Capriola-Regular.ttf';
		$image_width = 400;
		$dimensions = imagettfbbox($font_size, 20, $fontname, $string);
		
		return ceil(($image_width - $dimensions[4]) / 2);				
	}


	function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0){        
	    if ($spacing == 0)
	    {
	        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
	    }
	    else
	    {
	        $temp_x = $x;
	        for ($i = 0; $i < strlen($text); $i++)
	        {
	            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
	            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
	        }
	    }
	}

	function createVoucherImage(){
		//http://localhost/breakfastwrap/admin/createVoucherImage?usedate=2015-04-14&loop=1
		$loop = $this->input->get('loop',true);
		
		for($i=1;$i<=$loop;$i++){
			$this->load->helper('date');
			date_default_timezone_set('Asia/Jakarta'); 
			
			$txtemail = 'no-reply@mcdonalds.co.id';
			
			$datausedate = $this->input->get('usedate',true);
			$codeurl = 'generatevoucher';
			$ipaddress = $this->input->ip_address();
			$datenow = date("Y-m-d");
			$timenow = date('H:i:s');
			$vouchercode = strtoupper(substr(md5($txtemail.$datenow.$timenow.$i),0,12));


			//FONT FAMILY
			$fontname = 'assets/fonts/AkzidenzGroteskBECn.ttf';
			
			//SET JPG QUALITY
			$quality = 100;

			//DEFINE FILE NAME & LOCATION
			//$file = "assets/img/voucher/".$this->session->userdata('vouchercode').".jpg";	
			$voucherName = 	$datausedate." 00.00.00-".$vouchercode;
			$file = "assets/img/voucher/".$voucherName.".jpg";	

				//1. DEFINING BASE IMAGE
				$vouchersTemplate = 'assets/img/voucher-template-twitter.jpg';
				$im = imagecreatefromjpeg($vouchersTemplate);
				
				//2. EMBED VOUCHER CODE			
				$valuename = $vouchercode;
				$valuefontsize = '48';
				$valuecolor = imagecolorallocate($im, 88, 53, 18);
				$this->imagettftextSp($im, $valuefontsize, 0, 180, 777, $valuecolor, $fontname, $valuename, 10);

				//2.1 EMBED VOUCHER DATE
				$valuedate = strtoupper( date('d', strtotime($datausedate)) );
				$valuefontsize = '88';
				imagettftext($im, $valuefontsize, 0, 55, 690, $valuecolor, $fontname, $valuedate);
				
				//3. PRODUCE IMAGE
				imagejpeg($im, $file, 75);
							
			//return $file;	
		}
	}
}