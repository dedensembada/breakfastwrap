<!DOCTYPE html>
<html>
<head>    
	<meta charset="utf-8">    
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	<title><?php echo $title;?></title>   
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">    
	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">   
	<link href="<?php echo base_url();?>assets/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">    
	<link href="<?php echo base_url();?>assets/css/plugins/timeline/timeline.css" rel="stylesheet">	
	<link href="<?php echo base_url();?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">	
	<link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">
</head>
<body>	
	<div id="wrapper">        
		<?php $this->load->view('vadminmenu');?>        
		<div id="page-wrapper">            
			<div class="row">          
		      	<div class="col-lg-12">   
              		<h1 class="page-header">Video Code <a href="<?php echo base_url().'admin/addvideocode/'; ?>"><button class="btn btn-warning">ADD</button></a></h1>
            	</div>
            </div>	
			<div class="row"> 
	           	<div class="col-lg-12">
	               <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <!--<th>Youtube</th>-->
                                        <th>Code</th>
                                        <th>Status</th>
                                        <th>Click</th>
                                        <th>Register</th>
                                        <th>Date Create</th>
                                        <th>Expired Date</th>
                                       	<th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php 
										$counter=1;
										foreach($getVideoCode as $row):
									?>
									<tr>
										<td><?php echo $counter; ?></td>
										<!--<td><?php echo '<iframe width="200" height="auto" src="https://www.youtube.com/embed/'.$row->CODEYOUTUBE.'" frameborder="0" allowfullscreen></iframe>'; ?></td>-->
										<td><?php echo '<a target="_blank" href="'.base_url().'?ref='.$row->CODEURL.'">'.base_url().'?ref='.$row->CODEURL.'</a>'; ?></td>
										<td><?php if($row->CODEACTIVE==1){ echo 'Active';}else{ echo 'Not Active';} ?></td>
										<td>
											<?php
												$queryclick = $this->db->query("	SELECT * FROM TTRACKINGCODE
																			WHERE TTRACKINGCODE.TRACKINGCODEURL = '$row->CODEURL'
																			AND	TTRACKINGCODE.TRACKINGCODESTATUS = 'click'");
												echo $queryclick->num_rows();
										  	?>
										</td>
										<td>
											<?php
												$queryregister = $this->db->query("	SELECT * FROM TTRACKINGCODE
																			WHERE TTRACKINGCODE.TRACKINGCODEURL = '$row->CODEURL'
																			AND	TTRACKINGCODE.TRACKINGCODESTATUS = 'register'");
												echo $queryregister->num_rows();
										  	?>
										</td>
										<td><?php echo date("d M Y",strtotime($row->CREATEDATE));?></td>
										<td><?php echo date("d M Y",strtotime($row->EXPIREDDATE));?></td>
										<td>
											<!--<form action="<?php echo base_url();?>admin/deleteVideoCode/" method="post">
												<input type="hidden" name="codeID" value="<?php echo $row->CODEID; ?>">
												<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></button>
											</form>&nbsp;
											<form action="<?php echo base_url();?>admin/deleteVideoCode/" method="post">
												<input type="hidden" name="codeID" value="<?php echo $row->CODEID; ?>">
												<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span></button>
											</form>&nbsp;-->
											<form action="<?php echo base_url();?>admin/deleteVideoCode/" method="post">
												<input type="hidden" name="codeID" value="<?php echo $row->CODEID; ?>">
												<button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
											</form>
										</td>
									</tr>
									<?php 
										$counter++;
										endforeach;
									?>
                                </tbody>
                            </table>
                        </div>
                    </div>
	            </div>
	        </div>
	    </div>
    </div>	
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>   
 	<script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>    
 	<script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>	
 	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>    
 	<script src="<?php echo base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>    
 	<script src="<?php echo base_url();?>assets/js/sb-admin.js"></script>	
 	<script>
		$(document).ready(function() {
			$('#dataTables-example').dataTable();		
		});    
	</script>
</body>
</html>