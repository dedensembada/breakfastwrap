<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $title ; ?></title>
		<link rel="icon" type="image/jpg" href="<?php echo base_url().'assets/img/favicon.ico';?>">
		<meta name="description" content="Mc Donalds Breakfast Warp" />
		<meta name="keywords" content="Mc Donalds Breakfast Warp" />
		<meta name="author" content="Mc Donalds" />
		<!-- Bootstrap -->
		<script src="<?php echo base_url().'assets/'; ?>js/modernizr.custom.js"></script>
		<link href="<?php echo base_url().'assets/'; ?>css/typo.css" rel="stylesheet">
		<link href="<?php echo base_url().'assets/'; ?>css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url().'assets/'; ?>css/jquery.fancybox.css" rel="stylesheet">
		<link href="<?php echo base_url().'assets/'; ?>css/flickity.css" rel="stylesheet" >
		<link href="<?php echo base_url().'assets/'; ?>css/animate.css" rel="stylesheet">
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
		<link href="<?php echo base_url().'assets/'; ?>css/styles.css" rel="stylesheet">
		<link href="<?php echo base_url().'assets/'; ?>css/queries.css" rel="stylesheet">
		<!-- Facebook and Twitter integration -->
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!--[if lt IE 7]>
		<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->
		<!-- open/close -->
		<header>
			<section class="cover"></section>
			<section class="super">
				<div class="asset-cover">
					<img src="<?php echo base_url();?>assets/img/super.png" width="100%" alt="">
					<a class="mcd-link" target="_blank" href="http://www.mcdonalds.co.id">Kunjungi www.mcdonalds.co.id</a>
				</div>				
			</section>
			<section class="hero">
				<div class="container">
					<div class="row">
						<div class="panel col-md-10 col-md-offset-1 text-center">
							<div class="msg-wrapper">
								<div class="msg-img-wrapper">
									<img class="animated fadeInUp" src="<?php echo base_url();?>assets/img/breakfast-wrap-off.jpg" width="100%">							
								</div>
								<div class="message">
									<h1><?php echo $message1; ?></h1>
									<h2><?php echo $message2; ?></h2>
									<h3><?php echo $message3; ?></h3>
									<?php if( isset($error) == true){?>
										
										<a href="http://youtube.com/mcdonaldsid/watch?v=YBfaxC_pels" target="_blank" class="bf-link yt-link">Klik disini untuk menonton video</a>

									<?php } ?>	
								</div>
							</div>
							
							<?php if( isset($error) == false){?>
							
								<a href="https://www.facebook.com/notes/mcdonalds/syarat-dan-ketentuan-voucher-gratis-breakfast-wrap/1113987928627933" target="_blank" class="bf-link">Mau voucher Breakfast Wrap combo GRATIS?</a>
							
							<?php }?>
							
							<div style="clear:both"></div>
							<br>
							<div class="tnc-wrappers">
								<span class="tnc-text">Mekanisme:</span>
								<ul class="list-tnc">
									<li>Dapatkan Kupon GRATIS Breakfast Wrap dalam video <a href="http://youtube.com/mcdonaldsid/watch?v=YBfaxC_pels" target="_blank">"Pilihan Pagi"</a>.</li>
									<li>Dapatkan voucher GRATIS Breakfast Wrap dalam video <a href="http://youtube.com/mcdonaldsid/watch?v=YBfaxC_pels" target="_blank">"Pilihan Pagi"</a></li>
									<li>Voucher Breakfast Wrap hanya dapat diperoleh dari video <a href="http://youtube.com/mcdonaldsid/watch?v=YBfaxC_pels" target="_blank">"Pilihan Pagi"</a>, mulai tanggal 10-30 April 2015</li>
									<li>Temukan link tersembunyi yang ada pada video <a href="http://youtube.com/mcdonaldsid/watch?v=YBfaxC_pels" target="_blank">"Pilihan Pagi"</a> untuk mendapatkan 1 voucher dari total 100 voucher yang tersedia setiap harinya.<div><img src="<?php echo base_url();?>assets/img/yt-preview.jpg" alt="Youtube Preview"></div></li>
									<li>Masukan email kamu yang masih valid karena voucher akan dikirimkan ke email kamu.</li>
									<li>Tukarkan voucher tersebut ke store McDonald's terdekat.</li>
									<li>Voucher tidak berlaku di McDonald's:</li>
										<ul class="list-tnc2">
											<li>Lippo Karawaci</li>
											<li>Plaza Senayan</li>
											<li>Dufan</li>
											<li>Depok Mal</li>
											<li>Cempaka Mas</li>
											<li>Mall Taman Anggrek</li>
										</ul>
									<li>Voucher hanya berlaku dengan menunjukkan bukti email dan tidak berlaku dalam bentuk print out.
</li>
									<li>Satu orang HANYA bisa menukar satu voucher Breakfast Wrap. Jika menukarkan lebih dari satu voucher, maka yang berlaku hanya satu voucher Breakfast Wrap.</li>
								</ul>
							</div>
							<br>
							<div class="tnc-wrappers">
								<span class="tnc-text">Syarat dan ketentuan:</span>
								<ul class="list-tnc">
									<li>Satu email hanya berhak mendapatkan satu Voucher GRATIS Breakfast Wrap.</li>
									<li>Tersedia 100 voucher setiap harinya selama periode program.</li>
									<li>Voucher HANYA dapat digunakan pada tanggal yang tertera di voucher.</li>
									<li>Mekanisme dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu.</li>
								</ul>
							</div>
						</div>
					</div>
					
					
				</div>
			</section>
		</header>
		
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="<?php echo base_url().'assets/'; ?>js/min/toucheffects-min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="<?php echo base_url().'assets/'; ?>js/flickity.pkgd.min.js"></script>
		<script src="<?php echo base_url().'assets/'; ?>js/jquery.fancybox.pack.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?php echo base_url().'assets/'; ?>js/retina.js"></script>
		<script src="<?php echo base_url().'assets/'; ?>js/waypoints.min.js"></script>
		<script src="<?php echo base_url().'assets/'; ?>js/bootstrap.min.js"></script>
		<script src="<?php echo base_url().'assets/'; ?>js/min/scripts-min.js"></script>
		<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-61754655-1', 'auto');
		  ga('send', 'pageview');

		</script>


	</body>
</html>
