<?php
class Mdata extends CI_Model{
	
    function getCode($codeurl) {
        $this->db->from('TCODE');
        $this->db->where('CODEURL',$codeurl);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
        
    }

    function cekdatas($txtemail)
    {
        $this->db->from('TDATA');
        
        $this->db->where('DATAEMAIL',$txtemail);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    function cekdata($txtemail, $datadate)
    {
        $this->db->where('DATAEMAIL',$txtemail);
        $this->db->where('DATADATE',$datadate);
        return $this->db->get('TDATA');
    }
	
	function cekavailable($datenow)
    {
        $this->db->where('DATADATE',$datenow);
        return $this->db->get('TDATA');
    }

    function savedata($savedata) {
        $this->db->insert('TDATA', $savedata);
    }
    
    function savedatatracking($savedatatracking) {
        $this->db->insert('TTRACKINGCODE', $savedatatracking);
    }
    
    function saveVideoCode($datacontent) {
        $this->db->insert('TCODE', $datacontent);
    }
    
    function getVideoCode(){
        $this->db->from('TCODE');
        $this->db->order_by('CODEID','DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    
    function deleteVideoCode($id){
        $this->db->delete('TCODE', array('CODEID' => $id)); 
    }
    
    function getVoucherData(){
        $this->db->from('TDATA');
        $this->db->order_by('DATADATE','DESC');
        $this->db->order_by('DATATIME','DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    /*
	
	CREATE TABLE TCODE (
		CODEID INT NOT NULL AUTO_INCREMENT,
        CODEURL VARCHAR(255),
        CODEACTIVE INT,
        PRIMARY KEY (CODEID)
	)

    INSERT INTO TCODE (CODEURL, CODEACTIVE) VALUES ('be40287513d411b4e891339023a291cb','1');
    INSERT INTO TCODE (CODEURL, CODEACTIVE) VALUES ('0b7d5a9f24097cb0130fad5d8ca8d359','1');
    INSERT INTO TCODE (CODEURL, CODEACTIVE) VALUES ('a9c95c4432d076dbb30fa624e98da42f','1');

	CREATE TABLE TDATA (
        DATAID INT NOT NULL AUTO_INCREMENT,
        DATAEMAIL VARCHAR(255),
        DATAIPADDRESS VARCHAR(255),
        DATADATE DATE,
        DATATIME TIME,
        DATAVOUCHERCODE VARCHAR(255),
        DATAUSEDATE DATE,
        DATAUSETIMESTART TIME,
        DATAUSETIMEEND TIME,
        PRIMARY KEY (DATAID)
    );
    

	*/
}	
?>
